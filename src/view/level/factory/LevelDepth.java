package view.level.factory;

/**
 * Determines the depth of the player in the dungeon.
 */
public enum LevelDepth {

    /**
     * Ground floor textures.
     */
    ZERO,

    /**
     * First level of depth.
     */
    ONE,

    /**
     * Second level of depth.
     */
    TWO,

    /**
     * Final level of depth.
     */
    BOSS
}

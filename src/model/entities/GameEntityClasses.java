package model.entities;
/**
 * This Enum lists all the possible classes the game entities can have.
 */
public enum GameEntityClasses {

    /**
     * A consumable item.
     */
    CONSUMABLE,

    /**
     * The Knight pg.
     */
    KNIGHT,

    /**
     * The Archer pg.
     */
    ARCHER,

    /**
     * The Thief pg.
     */
    THIEF,

    /**
     * The Skeleton enemy.
     */
    SKELETON,

    /**
     * The Goblin enemy.
     */
    GOBLIN,
    
    /**
     * The Boss enemy.
     */
    BOSS
}
